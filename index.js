const config = require('./config.js');
console.log(`URL: ${config.url}\nport: ${config.port}\n`);

const controller = require('./control.js');
console.log(controller.controller().message + '\n');

const data = require('./data.js');
console.log(`name: ${data.name}\nweapon: ${data.weapon}\nweakness: ${data.weakness}\n`);